import { getRequest, postRequest, putRequest, deleteRequest } from './requests'

// const routePathBroadcast = 'stream/admin/broadcasts'
const routePathStream = 'stream/admin/streams'
const routePathStreamCats = 'stream/admin/broadcastCategories'

export default {
    async getAllBroadcasts(params) {
        return await getRequest('https://dev.fitliga.com/api/v0/stream/admin/broadcasts', { params })
    },
    // async getAllBroadcasts(params) {
    //     return await getRequest(`${routePathBroadcast}`, { params })
    // },


    async getAllStreams(params) {
        return await getRequest(`${routePathStream}`, { params })
    },

    async getCategories(params) {
        return await getRequest(`stream/broadcastCategories`, { params })
    },
    async createStreamCategory(data) {
        return await postRequest(`${routePathStreamCats}`, data)
    },
    // async getStreamCategory(id) {
    //     return await getRequest(`${routePathStreamCats}/${id}`)
    // },
    async updateStreamCategory(id, data) {
        return await putRequest(`${routePathStreamCats}/${id}`, data)
    },
    async deleteStreamCategory(id) {
        return await deleteRequest(`${routePathStreamCats}/${id}`)
    },
    async restoreStreamCategory(id) {
        return await putRequest(`${routePathStreamCats}/${id}/restore`)
    },
}
