import { getRequest, postRequest } from './requests'
const routePath = 'uploader/admin'

export default {
    async getMimetype() {
        return await getRequest(`${routePath}/mimetype`)
    },
    async getFiles() {
        return await getRequest(`${routePath}/file`)
    },
    async uploadFile(data) {
        return await postRequest(`${routePath}/azure`, data)
    },
}