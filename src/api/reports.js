import { getRequest } from './requests'
const routePath = 'report/admin'

export default {
    async getAll() {
        await getRequest(`${routePath}/userReports`)
    }
}